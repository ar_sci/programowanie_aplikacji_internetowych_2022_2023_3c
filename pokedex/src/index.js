import React from 'react';
import ReactDOM from 'react-dom/client';

import pokedex_data from 'assets/pokedex.json'
import types_data from 'assets/types.json'

import { View as PokemonView } from 'components/pokemon/view'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <PokemonView pokedex={pokedex_data} types={types_data} />
    </React.StrictMode>
);
