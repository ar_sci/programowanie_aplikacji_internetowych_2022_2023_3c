import './tile.css';

export const Tile = (props) => {
    const {id, name} = props;
    const path = `./images/${id.toString().padStart(3, '0')}.png`;

    return (
        <div className='pokemon_tile'>
            <img className='pokemon_tile_image' alt='' src={require(`${path}`)} />
            <div className='pokemon_tile_text'>{name}</div>
        </div>
    )
};
