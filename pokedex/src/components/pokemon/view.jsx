import { Tile } from "components/pokemon/tile";
import { useRef, useState } from "react";
import './view.css';

export const View = (props) => {
    const {pokedex, types} = props;
    const [filtered_name, set_filtered_name] = useState('');
    const filter_input = useRef();

    const handle_filter_input = () => {
        const value = filter_input.current.value;
        set_filtered_name(value);
    };

    let data = pokedex;
    data = data.filter((element) => {
        const pokemon_name = element.name.english.toLowerCase();
        return pokemon_name.includes(filtered_name);
    });

    return (
        <div>
            <div>
                <div>
                    Search: <input ref={filter_input} type="text" onChange={handle_filter_input} />
                </div>
            </div>
            <div className='pokemon_view'>
                {
                    data.map((item) => {
                        return (
                            <Tile id={item.id} name={item.name.english} />
                        );
                    })
                }
            </div>
        </div>
    )
};
