const sci = (() => {

    let dane = [
        1, "a", "b"
    ];

    const private = () => {
        console.log("private");
    };

    const public = () => {
        private();
        console.log("public", dane);
    };

    const call = () => {
        // private();
        // console.log("call");
        // const element = document.getElementById("element1");
        // const attributeValue = element.getAttribute("sci_pai");
        // console.log(attributeValue);
        // element.setAttribute("klucz", "wartosc");
        // element.removeAttribute("klucz");

        const root = document.getElementById("root");
        const el = document.createElement("input");
        el.setAttribute("type", "button");
        el.setAttribute("value", "Przycisk z JS");
        root.appendChild(el);
    };

    return {
        "call": call,
    };
})();
