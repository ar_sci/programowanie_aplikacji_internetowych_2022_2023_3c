# Wprowadzenie

1. Tworzenie pliku html:
    ``` html
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title>{TYTUŁ}</title>
        <script src="{ŚCIEŻKA_DO_PLIKU_JS}"></script>
        <script src="{ŚCIEŻKA_DO_PLIKU_JS}"></script>
        <script src="{ŚCIEŻKA_DO_PLIKU_JS}"></script>
    </head>
    <body>
    </body>
        <script>
            {WYWOŁANIA_FUNKCJI_Z_PLIKÓW_JS}
            {MIESCE_NA_IMPLEMENTACJE_SKRYPTU}
        </script>
    </html>
    ```
2. Tworzenie pliku js
    - plik tekstowy o nazwie `main.js`
3. Dodanie przycisku do pliku html:
    ``` html
    <button onclick="{FUNKCJA}(event)">
    ```
4. Uzyskanie dostępu do elementu html:
    ``` js
    let element = document.getElementById("{id elementu}");
    element.innerText = "Text";
    ```
5. Tworzenie tablicy:
    ``` js
    let tab = [1, 2, 3, 4, 5];
    tab.forEach((item) => {
        console.log(item);
    });
    ```
6. Wykorzystanie operatorów:
    ```
    + => dodawanie
    - => odejmowanie
    * => mnożenie
    / => dzielenie
    % => modulo, reszta z dzielenia
    ```
7. Pętla for:
    ``` js
    let tab = [];
    for(let i = 0; i < 10; i++) {
        tab.push(i);
    }
    ```
8. Warunek if:
    ``` js
    let v = 3;
    if ((v % 2) === 0) {
        console.log("liczba jest parzysta")
    }
    ```

---

# Funkcje, które musisz znać

- `console.log` => wyświetla tekst w konsoli. => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/API/Console/log)

- [zmienna](https://developer.mozilla.org/pl/docs/Glossary/Variable), [tekst](https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/String), [funkcja](https://developer.mozilla.org/pl/docs/Glossary/Function), [obiekt](https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/Object), [tablica](https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/Array)

    ```js
  let zmienna_liczba = 1;
  let zmienna_tekst = "Tekst";
  let zmienna_funkcja = function(args) {};
  let zmienna_obiekt = {
      "klucz": wartość
  };
  let zmienna_tablica = [0, 1, 2];
  ```

- obiekt: `document` => dostęp do reprezentacji pliku HTML => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/API/Document) 
    - getElementById => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/API/Document/getElementById)
    - createElement => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/API/Document/createElement)

- obiekt: `element` => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/API/Element)
    - getAttribute => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/API/Element/getAttribute)
    - setAttribute => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/API/Element/setAttribute)
    - innerHTML => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/API/Element/innerHTML)
    - innerText => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/API/HTMLElement/innerText)

- obiekt: `array` => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/Array)
    - length
    - forEach
    - map
    - push
    - filter
    - Array
    - fill
    - includes
    - findIndex
    - splice

- obiekt: `object` => [dokumentacja](https://developer.mozilla.org/pl/docs/Web/JavaScript/Referencje/Obiekty/Object)
    - keys
    - values

----

# Tabela

Poniżej znajduje się kod html, który stworzy prostą tabelę na stronie www w przeglądarce.
```html
<table>
    <tr>
        <th>Firstname</th>
        <th>Surname</th>
    </tr>
    <tr>
        <td>Jan</td>
        <td>Kowalski</td>
    <tr>
    <tr>
        <td>Ania</td>
        <td>Kowalska</td>
    <tr>
</table>
```

Podgląd:\
![](../assets/tabela_preview_base.PNG)

---
---

Uzyskanie dostępu do elementu tabeli z poziomu java scriptu odbywa się poprzez wywołanie metody `getElementById` z obiektu `document`. Musisz oznaczyć swoją tabelę odpowiednim indentyfikatorem, aby móc uzyskać dostęp do tabeli.

```html
<table id="tabelka">
```

```js
let tabela = document.getElementById("tabelka");
console.log(tabela);
```

Podgląd:\
![](../assets/tabela_preview_getElementById.PNG)

*PS. Pamiętaj, aby uruchomić skrypt po utworzeniu tabeli w pliku HTML.*

```html
<table id="tabelka">
    <tr>
        <th>Firstname</th>
        <th>Surname</th>
    </tr>
    <tr>
        <td>Jan</td>
        <td>Kowalski</td>
    <tr>
    <tr>
        <td>Ania</td>
        <td>Kowalska</td>
    <tr>
</table>

<script src="./main.js"></script>
```

---
---

Dodawanie elementu do tabeli odbywa się w następujący sposób. (appendChild) \
1. Utwórz dynamicznie element, który będzie reprezentował wiersz tabeli: `tr` (table row)
    ```js
    let row = document.createElement("tr");
    ```

2. Utwórz dynamicznie element, który będzie reprezentował pierwszy element wiersza, czyli jest to kolumna z imieniem.
    ```js
    let row_name = document.createElement("td");
    ```

3. Utwórz dynamicznie element, który będzie reprezentował drugi element wiersza, czyli jest to kolumna z nazwiskiem.
    ```js
    let row_surname = document.createElement("td");
    ```

4. Wypełnij elementy danymi:
    ```js
    row_name.innerText = "Małgorzata";
    row_surname.innerText = "Kowalska";
    ```

5. Dodaj następujące elementy do wiersza, tak aby połączyć nowe elementy w jeden, większy. W tym momencie przypiszesz do rodzica (row) nowe elementy, dzieci (row_name, row_surname):
    ```js
    row.appendChild(row_name);
    row.appendChild(row_surname);
    ```

6. Teraz rodzica (row), musisz przypisać do następnego elementu, którym jest tabelka. W tym momencie rodzic (row) stanie się dzieckiem dla tabelki (tabela).
    - tabela (rodzic) => row (dziecko)
    - row (rodzic) => row_name, row_surname (dzieci)
    ```js
    tabela.appendChild(row);
    ```

Podgląd:\
![](../assets/tabela_preview_appendChild.PNG)

---
---

Dodawanie elementu do tabeli odbywa się w następujący sposób. (tBodies) \

1. Obiekt tabeli posiada właściwość `tBodies`, jest to kolekcja (tablica) elementów html. Tabela może posiadać kilka `tBody`, dlatego `tBodies` jest tablicą / kolekcją.
    ```js
    console.log(tabela.tBodies);
    ```

2. Dodawanie elementu odbywa się w następujący sposób:
    ```js
    tabela.tBodies[0].appendChild(row);
    ```

---
---

Usuwanie elementu z tabeli:

1. Wykorzystując metodę `deleteRow` oraz podając indeks.
    ```js
    tabela.deleteRow(3);
    ```

2. Wykorzystując informacje z obiektu `tBodies[0]`, który aktualnie zawiera wszystkie elementy tabeli.
    ```js
    tabela.tBodies[0].removeChild(item)
    ```
    item jest to element html

---
---

Kod źródłowy znajduje się pod linkiem: [link](https://gitlab.com/ar_sci/programowanie_aplikacji_internetowych_2022_2023_3c/-/tree/main/assets/tabela_source)
